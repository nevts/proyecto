﻿# Host: localhost  (Version: 5.6.16)
# Date: 2014-09-07 13:48:54
# Generator: MySQL-Front 5.3  (Build 4.136)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "pw_m_categoria"
#

DROP TABLE IF EXISTS `pw_m_categoria`;
CREATE TABLE `pw_m_categoria` (
  `CAT_IdCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `CAT_Descripcion` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`CAT_IdCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

#
# Data for table "pw_m_categoria"
#

INSERT INTO `pw_m_categoria` VALUES (1,'Seleccione Categoría'),(2,'Compra / Venta'),(3,'Películas'),(4,'Softwares / Programas'),(5,'Libros / Revistas'),(6,'Juegos de PC');

#
# Structure for table "pw_m_pais"
#

DROP TABLE IF EXISTS `pw_m_pais`;
CREATE TABLE `pw_m_pais` (
  `PAI_IdPais` int(11) NOT NULL AUTO_INCREMENT,
  `PAI_Descripcion` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`PAI_IdPais`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

#
# Data for table "pw_m_pais"
#

INSERT INTO `pw_m_pais` VALUES (1,'Ecuador'),(2,'Peru'),(3,'Chile'),(4,'Argentina'),(5,'Bolivia');

#
# Structure for table "pw_m_usuario"
#

DROP TABLE IF EXISTS `pw_m_usuario`;
CREATE TABLE `pw_m_usuario` (
  `USU_IdUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `USU_Correo` varchar(255) NOT NULL DEFAULT '',
  `USU_Nombre` varchar(255) NOT NULL DEFAULT '',
  `USU_Apellido` varchar(255) NOT NULL DEFAULT '',
  `USU_Usuario` varchar(255) NOT NULL DEFAULT '',
  `USU_Contraseña` varchar(255) NOT NULL DEFAULT '',
  `PAI_IdPais` int(11) NOT NULL DEFAULT '0',
  `USU_FecNacimiento` date NOT NULL DEFAULT '0000-00-00',
  `USU_FraseSeguridad` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`USU_IdUsuario`),
  KEY `PAI_IdPais` (`PAI_IdPais`),
  CONSTRAINT `pw_m_usuario_ibfk_1` FOREIGN KEY (`PAI_IdPais`) REFERENCES `pw_m_pais` (`PAI_IdPais`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "pw_m_usuario"
#


#
# Structure for table "pw_t_cabarchivo"
#

DROP TABLE IF EXISTS `pw_t_cabarchivo`;
CREATE TABLE `pw_t_cabarchivo` (
  `CAB_IdArchivo` int(11) NOT NULL AUTO_INCREMENT,
  `CAB_Descripcion` varchar(1000) NOT NULL DEFAULT '',
  `CAT_IdCategoria` int(11) NOT NULL DEFAULT '0',
  `CAB_Titulo` varchar(255) NOT NULL DEFAULT '',
  `USU_IdUsuario` int(11) NOT NULL DEFAULT '0',
  `CAB_Portada` varchar(800) DEFAULT NULL,
  `CAB_Enlace` varchar(1000) DEFAULT '',
  `CAB_Contraseñade` varchar(500) DEFAULT NULL,
  `CAB_Precio` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`CAB_IdArchivo`),
  KEY `CAT_IdCategoria` (`CAT_IdCategoria`),
  KEY `USU_IdUsuario` (`USU_IdUsuario`),
  CONSTRAINT `pw_t_cabarchivo_ibfk_3` FOREIGN KEY (`CAT_IdCategoria`) REFERENCES `pw_m_categoria` (`CAT_IdCategoria`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pw_t_cabarchivo_ibfk_4` FOREIGN KEY (`USU_IdUsuario`) REFERENCES `pw_m_usuario` (`USU_IdUsuario`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "pw_t_cabarchivo"
#


#
# Structure for table "pw_t_carrito"
#

DROP TABLE IF EXISTS `pw_t_carrito`;
CREATE TABLE `pw_t_carrito` (
  `CAR_IdCarrito` int(11) NOT NULL AUTO_INCREMENT,
  `USU_IdUsuario` int(11) NOT NULL DEFAULT '0',
  `CAB_IdArchivo` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CAR_IdCarrito`),
  KEY `USU_IdUsuario` (`USU_IdUsuario`),
  KEY `CAB_IdArchivo` (`CAB_IdArchivo`),
  CONSTRAINT `pw_t_carrito_ibfk_1` FOREIGN KEY (`USU_IdUsuario`) REFERENCES `pw_m_usuario` (`USU_IdUsuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pw_t_carrito_ibfk_2` FOREIGN KEY (`CAB_IdArchivo`) REFERENCES `pw_t_cabarchivo` (`CAB_IdArchivo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "pw_t_carrito"
#

