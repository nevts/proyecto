<?php
session_start();
if (!isset($_SESSION['userid'])){

	header("location:../login/login.php");
}
else{
	require "../conexion/conexion.php";
	include '../clases/operaciones/operaciones.php';
	require_once '../clases/pagineo/Zebra_Pagination.php';
	$operaciones = new operaciones();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Datos</title>
		<meta charset="ISO-8859-1">	
		<link rel="icon" href="../img/favicon.png" type="image/png" />
		<link rel="stylesheet" type="text/css" href="../css/estilo.css" media="screen,print" />
		<link rel="stylesheet" type="text/css" href="../css/zebra_pagination.css" media="screen,print" />
	
	</head>
	<body>
	
		<div id="imHeaderBg"> </div>		
	<div id="imPage">
		  <div id="imHeader"> </div>
			
	  <div id="imMnMn" class="auto">
	  
	    <div id="usuario"> 
	  <?php	 
	  $id=$_SESSION['userid']; 
	  $operaciones->mostrar_usuario('pw_m_usuario','USU_Nombre','USU_Apellido','USU_IdUsuario', $id)
	  ?>	 
	  </div>
	  
				<ul class="auto">
					<li id="imMnMnNode0">
						<a href="../principal/inicio.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Inicio</span>
							</span>
						</a>
					</li>
					<li id="imMnMnNode4">
						<a href="../posts/misposts.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Posts</span>
							</span>
						</a>
					</li>	
					<li id="imMnMnNode6" class="imMnMnCurrent"><?php if($operaciones->administrador('pw_m_usuario','USU_IdUsuario', $id, 'USU_Usuario')==1){?>
						<a href="../administrador/formulario.php"><?php } else{?>						
						<a href="../datos/datos.php"><?php }?>
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"> </span>Datos</span>
							</span>
						</a>
					</li><li id="imMnMnNode7">
						<a href="../carrito/mostrar_carrito.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Carrito</span>
							</span>
						</a>
					</li><li id="imMnMnNode5">
						<a href="../cerrar-sesion/cerrar-sesion.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"> </span>Cerrar sesion</span>
							</span>
						</a>
					</li>
				</ul>
	  </div>
			<div id="imContentGraphics2">
<div align="center"><br><br>
			<h1><font color="#00557F">USUARIOS REGISTRADOS</font></h1><br>
			</div>
			<div class="datagrid">
			<table border="2" align="center">	
			<thead><tr>
			<th>ID</th>
			<th>NOMBRE</th>
			<th>APELLIDO</th>
			<th>USUARIO</th>
			<th>ELIMINAR</th>
			</tr></thead>
			
			<?php
			$sql1="select count(*) cantidad from pw_m_usuario"; 			
			$rec1= mysql_query($sql1);
			$results1=mysql_fetch_array($rec1);
			$total_usuarios=$results1['cantidad'];
			$resultados=10;			
			
			$paginacion= new Zebra_Pagination();
			$paginacion->records($total_usuarios);
			$paginacion->records_per_page($resultados);
			$pagina_actual=(($paginacion->get_page())-1)*$resultados;
			
			$sql = "SELECT *FROM pw_m_usuario limit $pagina_actual , $resultados";
			$rec = mysql_query($sql);
			
			while($result = mysql_fetch_array($rec))
			{
				$idss=$result['USU_IdUsuario'];			
			?>
			<tbody>
			<tr>
			<td><?php echo $result['USU_IdUsuario']?></td>			
			<td><?php echo $result['USU_Nombre']?></td>
			<td><?php echo $result['USU_Apellido']?></td>
			<td><?php echo $result['USU_Usuario']?></td>
			<td><a class="elim" href="eliminar.php?idss=<?php echo $idss;?>">Eliminar</a></td>
			</tr></tbody>
			<?php }?>					
			</table></div><br><br>
			<?php $paginacion->labels('Anterior','Siguiente'); $paginacion->render();?>
			</div>			
	</div>
		
</body>
</html>
<?php }?>