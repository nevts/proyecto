<?php 
session_start();
if (!isset($_SESSION['userid'])){

	header("location:../login/login.php");
}
else{
	require "../conexion/conexion.php";
	include '../clases/operaciones/operaciones.php';	
	$operaciones = new operaciones();
	require_once '../clases/pagineo/Zebra_Pagination.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Carrito de Compras</title>
		<meta charset="ISO-8859-1">	
		<link rel="icon" href="../img/favicon.png" type="image/png" />
		<link rel="stylesheet" type="text/css" href="../css/estilo.css" media="screen,print" />
		<link rel="stylesheet" type="text/css" href="../css/zebra_pagination.css" media="screen,print" />
		
        </head>
	<body>
	<div id="imHeaderBg"> </div>		
	<div id="imPage">
		  <div id="imHeader"> </div>			
	  <div id="imMnMn" class="auto">
	  
	  <div id="usuario"> 
	  <?php	 
	  $id=$_SESSION['userid']; 
	  $operaciones->mostrar_usuario('pw_m_usuario','USU_Nombre','USU_Apellido','USU_IdUsuario', $id)
	  ?>	 
	  </div>    
				<ul class="auto">
					<li id="imMnMnNode0">
						<a href="../principal/inicio.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Inicio</span>
							</span>
						</a>
					</li>
						
					<li id="imMnMnNode4">
						<a href="../posts/misposts.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Posts</span>
							</span>
						</a>
					</li>					
					<li id="imMnMnNode6">
					<?php if($operaciones->administrador('pw_m_usuario','USU_IdUsuario', $id, 'USU_Usuario')==1){?>
						<a href="../administrador/formulario.php"><?php } else{?>						
						<a href="../datos/datos.php"><?php }?>
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"> </span>Datos</span>
							</span>
						</a>
					</li>
					<li id="imMnMnNode7" class="imMnMnCurrent">
						<a href="../carrito/mostrar_carrito.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Carrito</span>
							</span>
						</a>
					</li>	
					<li id="imMnMnNode5">
						<a href="../cerrar-sesion/cerrar-sesion.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"> </span>Cerrar sesion</span>
							</span>
						</a>
					</li>
				</ul>
	  </div>
			<div id="imContentGraphics2">
			
		<div align="center"><br><br>
			<h1><font color="#00557F">ART�CULOS DE CARRITO</font></h1><br>
			</div>
			<div class="datagrid">
			<table border="2" align="center">	
			<thead><tr>
			<th>VISTA PREVIA</th>
			<th>T�TULO</th>
			<th>PRECIO</th>
			<th>LISTO COMPRAR</th>
			<th>ELIMINAR</th>
			</tr></thead>
			
			<?php
			$suma=0.0;
			$sql1="select count(*) cantidad from pw_t_carrito"; 			
			$rec1= mysql_query($sql1);
			$results1=mysql_fetch_array($rec1);
			$total_articulos=$results1['cantidad'];
			$resultados=10;			
			
			$paginacion= new Zebra_Pagination();
			$paginacion->records($total_articulos);
			$paginacion->records_per_page($resultados);
			$pagina_actual=(($paginacion->get_page())-1)*$resultados;
			
			$sql = "SELECT *FROM pw_t_carrito limit $pagina_actual , $resultados";
			$rec = mysql_query($sql);
			
			while($result = mysql_fetch_array($rec))
			{
				$idcar=$result['CAR_IdCarrito'];	
				$idusupu=$result['USU_IdUsuario'];
				$idarchivo=$result['CAB_IdArchivo'];

				$sql2="select archivo.CAB_Portada,archivo.CAB_Titulo,archivo.CAB_Precio
				from pw_t_cabarchivo archivo where archivo.CAB_IdArchivo=$idarchivo";
				$rec2 = mysql_query($sql2);
				$results2=mysql_fetch_array($rec2);
				
				$portada=$results2['CAB_Portada'];
				$titulo=$results2['CAB_Titulo'];
				$precio=$results2['CAB_Precio'];
			?>
			<tbody>
			<tr>
			<td><img src="<?php if($portada==""){echo "../img/previa.jpg"; }else {echo "../posts/".$portada;}?>" heigth="32px" width="32px"></td>			
			<td><?php echo $titulo?></td>
			<td><?php echo $precio?></td>
			<td><a class="elim" href="../posts/ver.php?idss=<?php echo $idarchivo;?>&idcar=<?php echo $idcar;?>">ver</a></td>
			<td><a class="elim" href="operacion.php?idss=<?php echo $idarchivo;?>&eli=1&idcar=<?php echo $idcar;?>">Eliminar</a></td>
			</tr></tbody>
			<?php $suma=$suma+$precio; }?>					
			</table><br>
			<div align="center">
			<p><font color="#00557F" size="5px">Precio total en carrito: </font>
			<font color="blue" size="4px"><?php echo $suma; ?></font></p>
			
			</div>
			
			</div><br><br>
			<?php $paginacion->labels('Anterior','Siguiente'); $paginacion->render();?>
			
	</div>
			
				</body>
</html>
<?php }?>