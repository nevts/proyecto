<?php 
session_start();
if (!isset($_SESSION['userid'])){

	header("location:../login/login.php");
}
else{
	require "../conexion/conexion.php";
	include '../clases/operaciones/operaciones.php';
	$operaciones = new operaciones();
	if(isset($_GET['error']))
	{
		echo '<script language="javascript">alert("Aseg�rese de que todos los campos esten llenos");</script>';
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Editar Post</title>
		<meta charset="ISO-8859-1">	
		<link rel="icon" href="../img/favicon.png" type="image/png" />
		<link rel="stylesheet" type="text/css" href="../css/estilo.css" media="screen,print" />
	</head>
	<body>
	
		<div id="imHeaderBg"> </div>		
	<div id="imPage">
		  <div id="imHeader"> </div>
			
	  <div id="imMnMn" class="auto">
	   
	  <div id="usuario"> 
	  <?php	 
	  $id=$_SESSION['userid']; 
	  $operaciones->mostrar_usuario('pw_m_usuario','USU_Nombre','USU_Apellido','USU_IdUsuario', $id)
	  ?>	 
	  </div>
	  
	  	<ul class="auto">
					<li id="imMnMnNode0">
						<a href="../principal/inicio.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Inicio</span>
							</span>
						</a>
					</li>
					<li id="imMnMnNode4" class="imMnMnCurrent">
						<a href="../posts/misposts.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Posts</span>
							</span>
						</a>
					</li>	
					<li id="imMnMnNode6"><?php if($operaciones->administrador('pw_m_usuario','USU_IdUsuario', $id, 'USU_Usuario')==1){?>
						<a href="../administrador/formulario.php"><?php } else{?>						
						<a href="../datos/datos.php"><?php }?>
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"> </span>Datos</span>
							</span>
						</a>
					</li><li id="imMnMnNode7">
						<a href="../carrito/mostrar_carrito.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Carrito</span>
							</span>
						</a>
					</li><li id="imMnMnNode5">
						<a href="../cerrar-sesion/cerrar-sesion.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"> </span>Cerrar sesion</span>
							</span>
						</a>
					</li>
				</ul>
	  </div>
	  <div id="imContentGraphics4">
	  <?php 	 
	   if(isset($_GET['idss']))
			 {
			 	$idca=$_GET['idss'];
			 	$sql = "SELECT *FROM pw_t_cabarchivo where CAB_IdArchivo=$idca";
			 	$rec = mysql_query($sql);
			 	while($result = mysql_fetch_array($rec))
			 	{
			 		$idss=$result['CAB_IdArchivo'];
			 		$titulo=$result['CAB_Titulo'];
			 		$enlace=$result['CAB_Enlace'];
			 		$portada=$result['CAB_Portada'];
			 		$descripcion=$result['CAB_Descripcion'];
			 		$descomprimir=$result['CAB_Contrase�ade'];
			 		$categ=$result['CAT_IdCategoria'];
			 		$usu=$result['USU_IdUsuario'];
			 		$precio=$result['CAB_Precio'];
			 	}
			 
			 }			 
			 
	  ?>
	  <form action="operaciones.php" method="post" enctype="multipart/form-data">
	   
      <div id="archivo">
		      <h2><font color="#00557F">EDITAR POST</font></h2><br>
		      <div>
		 <label>Titulo: </label> <textarea name="titulo" rows="2" cols="30"><?php echo $titulo;?></textarea>
		 </div><br> 
		 
		       <div>
		 <label>Descripcion: </label> <textarea name="descripcion" rows="5" cols="40" maxlength="1000"><?php echo $descripcion;?></textarea>
		 </div><br> 
		 <div>
		  <p><font color="red">*Seleccionar categor�a nuevamente..</font></p>
		 <label>Categor�a:    </label><select name="categoria">
			<?php		
				$operaciones->select_option('pw_m_categoria','CAT_IdCategoria','CAT_Descripcion');
			?>		
			</select> 
						
		 </div><br>
		 <div>
		  <p><font color="silver"><font color="#00FF00">*</font>Si su archivo es de categor�a Compra / venta agregar precio, caso contrario dejar en blanco.</font></p>
		 <label>Costo: </label><br>
		 <textarea name="costo" rows="1" cols="10"><?php echo $precio;?></textarea><font color="silver">0.00</font>
		 </div><br>		 
		 <p><font color="silver"><font color="#00FF00">*</font>No olvide Clickear Enter entre enlace y enlace. PSDTA:Enlace y Pass no son necesarios en Compra / Venta.</font><p>
		 <div>
		 <label>Enlaces: </label><br>
		 <textarea name="enlaces" rows="10" cols="60"><?php echo $enlace;?></textarea>
		 </div><br>
		 <div>
		 <label>Pass: </label><br>
		 <textarea name="descomprimir" rows="1" cols="60"><?php echo $descomprimir;?></textarea>		 
		 </div><br>
		 <div id="guardar">
		 <input type="submit" name="guardar1" value="Guardar">
		 </div>	
		 <textarea name="categ" rows="1" cols="1"><?php echo $idca;?></textarea><font color="red">	
	  </div><br> 
 </form>
 </div>
      
 </div>
 </body>
</html>
<?php }?>
 